# VR TEAM GROUP REPORT #

## Leader / Group Assignments ##

It was decided that Sam would be the team leader for this assignment. 

The specific rooms had been allocated to us by Jacob, the project manager. After a visit to the Windermere Campus we then came to a decision on what each space was going to be utilised for.  
The decisions we made are as follows:  

Sam:

* Intro
* Site Navigation
* Floorplan

Tony:

* J140 Study Space
* J144 Staff Room
* J143 & J142 Mac Lab & Linux/PC Lab

Mark:

* J141 Lecture Room
* J121 Hallways
* J122 Server Room

Jeremy:

* J134 Breakout Room
* J135 Dissasembly Lab/Classroom
* J136 Dissasembly Lab/Classroom

Andrew:

* J124 VR Room
* J126 & Facilities

Regan:

* J103, J104 & J105 Lecture rooms

### Asset Allocation ####

We had a group discussion on who would create what assets during the development stage of the assessment as follows:

Sam:

Tony:

Mark:

Jeremy:

Andrew:

Regan:

We also have a list of other Assets to be created after the prescribed assets have been created. These will be on a first in first served basis as they are secondary to the main assessment.

## Project Tools ##

### Main Project Tool ###

We have been directed to use Unity as the tool to assemble our project as this is prescibed tool for this course. 

### Communication Tool ###

For the communication between team members we decided to stick with Slack as our communication tool. We did look at using an instant messaging service such as Viber. 
However as all memebers of the group already had a slack profile established and both the tutor (Jeff) and project manager (Jacob) were already set up with slack and part of the pre-established VR channel we decided it would be easier to continue to use this platform. 
Also Slack easily allows the sharing of image files, links and other documents. 

### Project Management ###

For managing our project we looked at using OpenProject as our tool. However after it was set up we decided that it was far too complicated for the needs of our project. 
We then found a much simpler project management tool called trello that will make it much simpler for us to keep details on who has created what assets. 

### Wireframing Tool ###

Wireframing for a VR application is a difficult prospect because traditional wireframes don't apply as they only show a 2D representation of what you will see. We considered doing 3D hand drawn wireframes for each of the spaces allocated to each person. 
However after deliberation we decided that this would be an astronomical amount of work as you would need multiple views per room to get a complete feeling of the spaces. 
After careful consideration we decided that a top down & roof floor plan would be the best way to give an overview of the layout of the spaces. We initially decided that these would also be hand drawn but for consistency between each person's spaces we decided that visio would be a better tool. 
This is because Visio has an inbult office layout designer that allows us to create a consistent flow through each space as everyone has access to the same set of tools. 

### Floorplan Creation Tool ###

To create the floorplan for the project we were initially going to use Unity as this is the tool we will build the project in. However after consideration by Sam, who is doing the basic floorplan, we decided that Unity was not sufficient because there is no accurate measure of scale when creating assets.  
We decided to make the floorplan in an external tool an import it as an object into Unity. The tool we decided to use is Sketchup. This program allows you to make floors and walls to scale with accurate measurements. This is important as we want this to be a realistic representation of the space we are modelling. 

### Asset Creation Tool ###

As Unity only has the ability to create basic objects within the actual program we have decided to use another program to create our game assets. 
We have chosen to use Blender as it is a free tool that will allow us to make more complex 3D assets. We chose it over any other possible free 3D development software because it is a program that has a good track record and members of the group have previous experience with it. 

### API Integration ###

At this stage in the development process we have decided to integrate the Facebook API and included it on one or potentially multiple interactable monitors throughout the application. 

## Resources ## 

Our project manager Jacob provided us with a list of useful resources that can be found [here](https://docs.google.com/document/d/12-wGjRyvu1lFRLr-Qcve3LIyD6EV0uQb7AQZKg6ypAk/edit)  
Other resources we will use include:  

* VRTK documentation and youtube tutorials [VRTK](https://vrtoolkit.readme.io/)
* Unity as this is the prescribed tool to create the VR project [Unity](https://unity3d.com/)
* Unity Tutorials & Documentation [Unity](https://unity3d.com/learn/tutorials) 
* Texture Packs from the Unity Asset Store [Asset Store](https://www.assetstore.unity3d.com/en/)
* Floor plan from visit to Windermere Campus [Photos](https://www.dropbox.com/sh/owqoj6myffj0qlr/AAAzwktnAvWyjVtZoRT1R1-2a?dl=0)
* Reference Images taken at Windermere Campus [Photos](https://www.dropbox.com/sh/owqoj6myffj0qlr/AAAzwktnAvWyjVtZoRT1R1-2a?dl=0)
* Multiple Physical Site Visits



