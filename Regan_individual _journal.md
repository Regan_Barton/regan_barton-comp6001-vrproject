#Individual Journal

##Report

We met up and went to the windemere campus to have a look around at their room layouts to see what they looked like and to get an idea of what to do.  
I was assigned J102, J103, J104, J105 and J106 with J102 and J106 being smaller rooms and J101 connecting the other 3 rooms.  
The assets i was assigned to create apart from my room was Desks and Chairs, when we went for the trip Jeremy took multiple photos of all the chairs and desks I will have to create.  

I have chosen this as my floor plan: 


* J103, This room has 2 columns of desks with 2 desks on a row and 3 seats at each desk all facing side of the class with a few round desks at the back for group work or more quiet work if needed. I have chosen this layout because it is similar to the real layout and this room is the main lecture room for when the tutors are giving the lectures and need students to watch the projector screen.

* J104, This room has 5 desks with 6 computers on each facing sideways from the front projector this allows students to sit in their groups or with friends at computers while being able to turn to face the teacher when he needs to talk.

* J105, This room has multiple round desks for a dedicated working room with the roller televisions for use incase any students need to use it for their work. This room is dedicated to work in incase students need a more casual enviroment in which to work.

* J102, This room is made to be a quiet working area for use by J105 students and has a table stretching around the outside with computers lining the table, this room connects to J105 and is there for use for students that need access to a computer since J105 only has the roller televisions avaliable.

* J106, This room connects from J103 and is only used as a small storage room since it is too small and dark to be used for anything else.  


##Asset creation  

I am using the software called 'Blender' to create all my assets, I chose this software because i have used it in the past briefly and I am already familiar with it and it can export models quite easily to unity for testing.